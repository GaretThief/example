<?php

namespace App\DataProviders;

use App\DataProviders\Exceptions\CurlException;
use App\DataProviders\Interfaces\IAPIProvider;

abstract class BaseAPIProvider implements IAPIProvider
{
    public $config = [];

    protected $apiParams = [];

    protected $cacheEnable = false;
    protected $cachePeriod = 1;

    public function __construct()
    {
        $this->init();
    }

    protected function init()
    {
    }

    public function getConfigValue(string $key, $default = null)
    {
        return $this->config[$key] ?? $default;
    }

    public function setAPIParams(array $params)
    {
        $this->apiParams = $params;
        return $this;
    }

    public function setAPIParam(string $paramKey, $value)
    {
        $this->apiParams[$paramKey] = $value;
        return $this;
    }

    public function getAPIParam(string $paramKey)
    {
        return $this->apiParams[$paramKey] ?? null;
    }

    public function setCacheEnable(bool $value = true)
    {
        $this->cacheEnable = $value;
        return $this;
    }

    public function setCachePeriod(int $value)
    {
        $this->cachePeriod = $value;
        return $this;
    }

    protected function buildGetParams(array $availableParams): string
    {
        $params = [];
        foreach ($availableParams as $paramKey) {
            $paramValue = $this->getAPIParam($paramKey);

            if ($paramValue) {
                $params[] = $paramKey . '=' . $paramValue;
            }
        }

        return implode('&', $params);
    }

    protected function cacheMethod(string $key, int $period, \Closure $callback)
    {
        return \Cache::remember($key, $period, $callback);
    }

    /**
     * @param string $apiUrl
     * @throws CurlException
     * @return string
     */
    protected function sendCurl(string $apiUrl): string
    {
        $curlCallback = function () use ($apiUrl) {
            try {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $apiUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
            } catch (\Exception $e) {
                throw new CurlException($e->getMessage(), $e->getCode());
            }

            return $result;
        };

        if ($this->cacheEnable) {
            return $this->cacheMethod($apiUrl, $this->cachePeriod, $curlCallback);
        }
        return $curlCallback();
    }
}
