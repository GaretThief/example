<?php

namespace App\DataProviders\Cryptocompare;

class ExchangeProvider extends BaseProvider
{
    public function getAPIUrl(): string
    {
        return $this->getAPIUrlByFunction(self::API_FUNCTION_ALL_EXCHANGES);
    }
}
