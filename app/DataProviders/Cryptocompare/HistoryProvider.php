<?php

namespace App\DataProviders\Cryptocompare;

abstract class HistoryProvider extends BaseProvider
{
    public $fromCurrency;
    public $toCurrency;
    public $periodTime;

    public function __construct(
        string $fromCurrency,
        string $toCurrency,
        int $periodTime
    ) {
        parent::__construct();

        $this->fromCurrency = $fromCurrency;
        $this->toCurrency = $toCurrency;
        $this->periodTime = $periodTime;
    }

    public function init()
    {
        parent::init();
        $this->setCacheEnable(false);
    }

    protected function getAvailableParams(): array
    {
        return [
            self::API_PARAMETER_EXCHANGE_NAME,
            self::API_PARAMETER_FROM_SYMBOL,
            self::API_PARAMETER_TO_SYMBOL,
            self::API_PARAMETER_EXTRA_PARAMS,
            self::API_PARAMETER_SIGN,
            self::API_PARAMETER_TRY_CONVERSION,
            self::API_PARAMETER_AGGREGATE,
            self::API_PARAMETER_LIMIT,
            self::API_PARAMETER_TO_TIMESTAMP,
        ];
    }

    public function getAPIParam(string $paramKey)
    {
        $value = parent::getAPIParam($paramKey);

        if ($value !== null) {
            return $value;
        }

        switch ($paramKey) {
            case self::API_PARAMETER_FROM_SYMBOL: return $this->fromCurrency;
            case self::API_PARAMETER_TO_SYMBOL: return $this->toCurrency;
            case self::API_PARAMETER_LIMIT: return $this->periodTime;
            case self::API_PARAMETER_EXCHANGE_NAME: return self::DEFAULT_EXCHANGE_NAME;

            default: return null;
        }
    }
}
