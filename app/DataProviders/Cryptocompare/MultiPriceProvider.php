<?php

namespace App\DataProviders\Cryptocompare;

class MultiPriceProvider extends BaseProvider
{
    const MAX_FROM_SYMBOLS_LENGTH = 300;

    public $fromSymbols;
    public $toSymbols;
    public $exchange;

    public function __construct(
        array $fromSymbols,
        array $toSymbols,
        string $exchange = self::DEFAULT_EXCHANGE_NAME
    ) {
        parent::__construct();

        $this->fromSymbols = $fromSymbols;
        $this->toSymbols = $toSymbols;
        $this->exchange = $exchange;
    }

    protected function getAvailableParams(): array
    {
        return [
            self::API_PARAMETER_FROM_SYMBOLS,
            self::API_PARAMETER_TO_SYMBOLS,
            self::API_PARAMETER_EXCHANGE_NAME,
            self::API_PARAMETER_EXTRA_PARAMS,
            self::API_PARAMETER_SIGN,
            self::API_PARAMETER_TRY_CONVERSION,
        ];
    }

    public function getAPIUrl(): string
    {
        return $this->getAPIUrlByFunction(self::API_FUNCTION_PRICE_MULTI_FULL);
    }

    public function getData(): array
    {
        $fromSymbols = $this->fromSymbols;
        $fromSymbolsUrl = implode(',', $fromSymbols);
        $fSymbChunkCount = ceil(strlen($fromSymbolsUrl) / self::MAX_FROM_SYMBOLS_LENGTH);
        $fSymbChunkSize = ceil(count($fromSymbols) / $fSymbChunkCount);
        $fSymbolsCuncks = array_chunk($fromSymbols, $fSymbChunkSize);

        $data = [];
        foreach ($fSymbolsCuncks as $fSymbolsCunck) {
            $this->fromSymbols = $fSymbolsCunck;
            $data = array_merge_recursive(parent::getData(), $data);
        }
        return $data;
    }

    public function getAPIParam(string $paramKey)
    {
        $value = parent::getAPIParam($paramKey);

        if ($value !== null) {
            return $value;
        }

        switch ($paramKey) {
            case self::API_PARAMETER_FROM_SYMBOLS: return implode(',', $this->fromSymbols);
            case self::API_PARAMETER_TO_SYMBOLS: return implode(',', $this->toSymbols);
            case self::API_PARAMETER_EXCHANGE_NAME: return $this->exchange ?? self::DEFAULT_EXCHANGE_NAME;

            default: return null;
        }
    }
}
