<?php

namespace App\DataProviders\Cryptocompare;

class MultiPricesFormatter
{
    public function format(array $responsePrices): array
    {
        $rawData = [];

        foreach ($responsePrices as $responsePrice) {
            if (!is_array($responsePrice)) {
                continue;
            }

            $responseStatus = $responsePrice[BaseProvider::RESPONSE_FIELD_RESPONSE] ?? BaseProvider::RESPONSE_SUCCESS;

            if (
                $responseStatus === BaseProvider::RESPONSE_SUCCESS
                && array_key_exists(BaseProvider::RESPONSE_FIELD_RAW, $responsePrice)
                && array_key_exists(BaseProvider::RESPONSE_FIELD_DISPLAY, $responsePrice)
            ) {
                $rawData = array_merge_recursive($rawData, $responsePrice[BaseProvider::RESPONSE_FIELD_RAW]);
            }
        }

        return $rawData;
    }
}
