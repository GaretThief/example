<?php

namespace App\DataProviders\Exceptions\Cryptocompare;

class NotFoundExchangeException extends \Exception
{
}
