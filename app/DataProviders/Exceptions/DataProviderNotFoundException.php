<?php

namespace App\DataProviders\Exceptions;

class DataProviderNotFoundException extends \Exception
{
}
