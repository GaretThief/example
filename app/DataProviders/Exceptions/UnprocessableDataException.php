<?php

namespace App\DataProviders\Exceptions;

class UnprocessableDataException extends \Exception
{
}
