<?php

namespace App\DataProviders;

class InsensitiveArrayFormatter
{
    const CHANGE_KEYS = 0b01;
    const CHANGE_VALUES = 0b10;

    const CHANGE_KEYS_VALUES = self::CHANGE_KEYS | self::CHANGE_VALUES;

    const FORMAT_UPPER = 1;
    const FORMAT_LOWER = 2;

    public $formatType;
    public $change;

    public function __construct(int $formatType = self::FORMAT_UPPER, int $change = self::CHANGE_KEYS_VALUES)
    {
        $this->formatType = $formatType;
        $this->change = $change;
    }

    public function format(array $data): array
    {
        $format = $this->formatType;

        if ($this->change & self::CHANGE_KEYS) {
            $data = array_change_key_case($data, $format === self::FORMAT_UPPER ? CASE_UPPER : CASE_LOWER);
        }

        if ($this->change & self::CHANGE_VALUES) {
            if ($format === self::FORMAT_UPPER) {
                $recursiveCallback = function (&$item) {
                    $item = strtoupper($item);
                };
            } else {
                $recursiveCallback = function (&$item) {
                    $item = strtolower($item);
                };
            }

            array_walk_recursive($data, $recursiveCallback);
        }

        return $data;
    }
}
