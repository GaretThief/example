<?php

namespace App\DataProviders\Interfaces;

interface IAPIProvider
{
    public function getAPIUrl(): string;

    public function setAPIParams(array $params);

    public function setAPIParam(string $paramKey, $value);

    public function getAPIParam(string $paramKey);

    public function getConfigValue(string $key, $default = null);

    public function getData();
}
