<?php

namespace App\DataProviders\Interfaces;

interface IFormatter
{
    public function format($data);
}
