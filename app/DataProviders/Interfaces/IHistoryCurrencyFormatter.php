<?php

namespace App\DataProviders\Interfaces;

interface IHistoryCurrencyFormatter extends IFormatter
{
    public function format($data, int $aggregate = null);
}
