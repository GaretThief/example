<?php

namespace App\Http\Controllers;

use Response;
use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const JS_APP_PATH = 'assets/js/app-dist';
    const JS_ROUTES_PATH = self::JS_APP_PATH . '/routes';

    protected function getJsAppPath($routeName = null)
    {
        $routeName = $routeName ?? \Route::currentRouteName();
        return self::JS_ROUTES_PATH . '/' . $routeName . '.js';
    }

    public function appendAppEntryPoint($routeName = null)
    {
        \View::share('jsAppPath', $this->getJsAppPath($routeName));
    }

    protected function getFailResponseByException(Exception $exception)
    {
        return Response::json([
            'success' => false,
            'message' => $exception->getMessage(),
        ]);
    }

    protected function getSuccessResponse($data)
    {
        $data = is_array($data) ? $data : ['data' => $data];

        return Response::json(array_merge($data, [
            'success' => true,
        ]));
    }
}
