<?php

namespace App\Http\Controllers;

use App;
use Request;
use Exception;
use App\Repositories\Eloquent\Models\Blacklist;

class CurrencyAPIController extends Controller
{
    public function getHistoryChartData(string $fromCurrency, string $toCurrency, string $periodType)
    {
        $currencyPriceService = new App\Services\CurrencyPriceService();

        try {
            $chartData = $currencyPriceService->getCryptocompareHistoryChartData(
                $fromCurrency,
                $toCurrency,
                $periodType
            );

            return $this->getSuccessResponse([
                'chartData' => $chartData,
                'chartPeriodType' => $periodType,
            ]);
        } catch (Exception $e) {
            return $this->getFailResponseByException($e);
        }
    }

    public function getPairsByExchange(string $exchange = null)
    {
        $exchangeService = new App\Services\CurrencyExchangeService();
        $currencyPriceService = new App\Services\CurrencyPriceService();

        try {
            $pairs = $exchangeService->getPairsByExchange($exchange);
            $blacklist = Blacklist::getPairsByExchangeName($exchange);
            if ($blacklist) {
                $pairs = $currencyPriceService->reversePairs($pairs);
                $pairs = $currencyPriceService->filterOutdatedPairs($pairs, $exchange, $blacklist);
                $pairs = $currencyPriceService->reversePairs($pairs);
            }
            return $this->getSuccessResponse(['pairs' => $pairs]);
        } catch (Exception $e) {
            return $this->getFailResponseByException($e);
        }
    }

    public function getPairsPriceByExchange()
    {
        $pairs = (array) Request::post('pairs', []);
        $exchange = (string) ucfirst(Request::post('exchange'));
        $currencyPriceService = new App\Services\CurrencyPriceService();
        $blacklist = Blacklist::getPairsByExchangeName($exchange);
        $pairs = $currencyPriceService->filterOutdatedPairs($pairs, $exchange, $blacklist);

        try {
            $prices = $currencyPriceService->getPairsPriceByExchange($pairs, $exchange);

            return $this->getSuccessResponse([
                'prices' => $prices,
                'exchange' => $exchange,
            ]);
        } catch (Exception $e) {
            return $this->getFailResponseByException($e);
        }
    }
}
