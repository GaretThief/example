<?php

namespace App\Repositories\Eloquent\Models\CryptoAPI;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Repositories\Eloquent\Models\CryptoAPI\CryptocomparePrice
 *
 * @property int $time
 * @property float $close
 * @property float $high
 * @property float $low
 * @property float $open
 * @property float $volumefrom
 * @property float $volumeto
 * @mixin \Eloquent
 */
class CryptocomparePrice extends Model
{
    const PERIOD_TYPE_HOUR = 'hour';
    const PERIOD_TYPE_DAY = 'day';
    const PERIOD_TYPE_WEEK = 'week';
    const PERIOD_TYPE_MONTH = 'month';
    const PERIOD_TYPE_3MONTH = '3month';
    const PERIOD_TYPE_6MONTH = '6month';
    const PERIOD_TYPE_YEAR = 'year';

    const PERIOD_TIME_HOUR = 60;
    const PERIOD_TIME_DAY = 24;
    const PERIOD_TIME_WEEK = 7;
    const PERIOD_TIME_MONTH = 30;
    const PERIOD_TIME_3MONTH = 30 * 3;
    const PERIOD_TIME_6MONTH = 30 * 6;
    const PERIOD_TIME_YEAR = 364;

    protected $fillable = ['time', 'close', 'high', 'low', 'open', 'volumefrom', 'volumeto'];
}
