<?php

namespace App\Services;

use App\DataProviders\Cryptocompare\ExchangeProvider;
use App\DataProviders\Exceptions\Cryptocompare\NotFoundExchangeException;
use App\DataProviders\Exceptions\ExchangeNotFoundException;

class CurrencyExchangeService
{
    public function getPairsByExchange(string $exchange = null): array
    {
        if (!$exchange) {
            throw new ExchangeNotFoundException('The exchanges name is not set');
        }
        $exchangeProvider = $this->getExchangeProvider();
        $allExchanges = $exchangeProvider->getData();
        $exchange = ucfirst(strtolower($exchange));
        if (!array_key_exists($exchange, $allExchanges)) {
            throw new NotFoundExchangeException('Exchange [' . $exchange . '] not found.');
        }
        return $allExchanges[$exchange];
    }

    public function searchSensitiveExchangeName(string $searchExchange): string
    {
        $exchangeProvider = $this->getExchangeProvider();
        $allExchanges = $exchangeProvider->getData();

        foreach ($allExchanges as $exchange => $pairs) {
            if (strtoupper($exchange) === strtoupper($searchExchange)) {
                return $exchange;
            }
        }
        return $searchExchange;
    }

    /**
     * @return ExchangeProvider
     * @codeCoverageIgnore
     */
    public function getExchangeProvider()
    {
        return new ExchangeProvider();
    }
}
