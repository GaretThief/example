<?php
Route::get('currency-api/get-history-chart-data/{fromCurrency}/{toCurrency}/{periodType}', 'CurrencyAPIController@getHistoryChartData')->name('currency.get-history-chart-data');
Route::get('currency-api/get-pairs-by-exchange/{exchange?}', 'CurrencyAPIController@getPairsByExchange')->name('currency.get-pairs-by-exchange');
Route::post('currency-api/get-pairs-price-by-exchange', 'CurrencyAPIController@getPairsPriceByExchange')->name('currency.get-pairs-price-by-exchange');
