<?php

namespace Tests\Unit\Service;

use App\DataProviders\Cryptocompare\ExchangeProvider;
use App\DataProviders\Exceptions\Cryptocompare\NotFoundExchangeException;
use App\DataProviders\Exceptions\ExchangeNotFoundException;
use App\Services\CurrencyExchangeService;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

/**
 * Class CurrencyExchange
 * @package Tests\Unit\Service
 * @group CurrencyExchangeTest
 */
class CurrencyExchangeTest extends TestCase
{
    /**
     * @param ExchangeProvider $exchangeProvider
     * @param $exchange
     * @param callable $expectedResult
     * @group CurrencyExchangeTest_getPairsByExchange
     * @dataProvider providerGetPairs
     */
    public function testGetPairsByExchange(ExchangeProvider $exchangeProvider, $exchange, callable $expectedResult)
    {
        /** @var MockObject|CurrencyExchangeService $mockCurrencyExchangeService */
        $mockCurrencyExchangeService = $this->getMockCurrencyExchangeService($exchangeProvider);

        try {
            $result = $mockCurrencyExchangeService->getPairsByExchange($exchange);
            $expectedResult($result);
        } catch (\Exception $e) {
            $expectedResult($e);
        }
    }

    public function getMockCurrencyExchangeService($exchangeProvider)
    {
        /** @var MockObject|CurrencyExchangeService $mockCurrencyExchangeService */
        $mockCurrencyExchangeService = $this
            ->getMockBuilder(CurrencyExchangeService::class)
            ->setMethods([
                'getExchangeProvider',
            ])
            ->getMock();

        $mockCurrencyExchangeService
            ->expects($this->any())
            ->method('getExchangeProvider')
            ->willReturn($exchangeProvider);

        return $mockCurrencyExchangeService;
    }

    public function getMockExchangeProvider($data)
    {
        /** @var MockObject|ExchangeProvider $mockExchangeProvider */
        $mockExchangeProvider = $this
            ->getMockBuilder(ExchangeProvider::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getData'
            ])
            ->getMock();

        $mockExchangeProvider
            ->expects($this->any())
            ->method('getData')
            ->willReturn($data);

        return $mockExchangeProvider;
    }

    public function providerGetPairs()
    {
        return [
            [
                'exchangeProvider' => $this->getMockExchangeProvider(['exchange_test' => 'value']),
                'exchange' => null,
                'expectedResult' => function ($error) {
                    $this->assertTrue($error instanceof ExchangeNotFoundException);
                }
            ],
            [
                'exchangeProvider' => $this->getMockExchangeProvider(['exchange_test' => 'value']),
                'exchange' => 'Exchange_test',
                'expectedResult' => function ($error) {
                    $this->assertTrue($error instanceof NotFoundExchangeException);
                }
            ],
            [
                'exchangeProvider' => $this->getMockExchangeProvider(['Exchange_test' => ['value' => []]]),
                'exchange' => 'Exchange_test',
                'expectedResult' => function ($result) {
                    $this->assertArraySubset(['value' => []], $result);
                }
            ]
        ];
    }

    /**
     * @param ExchangeProvider $exchangeProvider
     * @param string $searchExchange
     * @param callable $expectedResult
     * @group CurrencyExchangeTest_searchSensitiveExchangeName
     * @dataProvider providerSearchSensitiveName
     */
    public function testSearchSensitiveExchangeName(ExchangeProvider $exchangeProvider, string $searchExchange, callable $expectedResult)
    {
        /** @var MockObject|CurrencyExchangeService $mockCurrencyExchangeService */
        $mockCurrencyExchangeService = $this->getMockCurrencyExchangeService($exchangeProvider);

        $result = $mockCurrencyExchangeService->searchSensitiveExchangeName($searchExchange);
        $expectedResult($result);
    }

    public function providerSearchSensitiveName()
    {
        return [
            [
                'exchangeProvider' => $this->getMockExchangeProvider(['exchange' => 'value']),
                'searchExchange' => 'exchange',
                'expectedResult' => function ($result) {
                    $this->assertEquals('exchange', $result);
                }
            ],
            [
                'exchangeProvider' => $this->getMockExchangeProvider(['exchange' => 'value']),
                'searchExchange' => 'exchange_test',
                'expectedResult' => function ($result) {
                    $this->assertEquals('exchange_test', $result);
                }
            ],
        ];
    }
}
