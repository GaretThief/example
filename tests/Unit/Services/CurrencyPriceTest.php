<?php

namespace Tests\Unit\Service;

use App\DataProviders\Cryptocompare\ExchangeProvider;
use App\Repositories\Eloquent\Models\Blacklist;
use App\Services\CurrencyPriceService;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

/**
 * Class CurrencyPriceTest
 * @package Tests\Unit\Service
 * @group CurrencyPriceTest
 */
class CurrencyPriceTest extends TestCase
{
    /**
     * @param ExchangeProvider $exchangeProvider
     * @group CurrencyTest_getPairsPricesByExchanges
     * @dataProvider  providerPairsPrices
     */
    public function testGetPairsBlacklist(ExchangeProvider $exchangeProvider)
    {
        /* @var MockObject|ExchangeProvider $exchangeProvider */
        $exchangeProvider
            ->expects($this->once())
            ->method('getData')
            ->willReturn(array());

        /** @var MockObject|CurrencyPriceService $mockCurrencyPriceService */
        $mockCurrencyPriceService = $this
            ->getMockBuilder(CurrencyPriceService::class)
            ->setMethods([
                'getExchangeProvider',
                'reversePairsInExchanges',
                'getOutdatedPairs',
                'saveDataInBlacklist'
            ])
            ->getMock();

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('getExchangeProvider')
            ->willReturn($exchangeProvider);

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('reversePairsInExchanges')
            ->willReturn([]);

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('getOutdatedPairs')
            ->willReturn([]);

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('saveDataInBlacklist')
            ->willReturn(true);

        $array = $mockCurrencyPriceService->getPairsBlacklist([], 'test_exchange');
        $this->assertTrue(is_array($array));
    }

    public function getExchangeProvider()
    {
        $mockExchangeProvider = $this
            ->getMockBuilder(ExchangeProvider::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getData'
            ])
            ->getMock();

        return $mockExchangeProvider;
    }

    public function providerPairsPrices()
    {
        return [
            [
                '$exchangeProvider' => $this->getExchangeProvider()
            ]
        ];
    }

    /**
     * @group CurrencyPriceTest_ReversePairsInExchanges
     */
    public function testReversePairsInExchanges()
    {
        $exchanges = array('test_exchange' => []);
        /** @var MockObject|CurrencyPriceService $mockCurrencyPriceService */
        $mockCurrencyPriceService = $this
            ->getMockBuilder(CurrencyPriceService::class)
            ->setMethods([
                'reversePairs',
            ])
            ->getMock();

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('reversePairs')
            ->willReturn([]);

        $array = $mockCurrencyPriceService->reversePairsInExchanges($exchanges);
        $this->assertTrue(is_array($array));
    }

    /**
     * @group CurrencyPriceTest_ReversePairs
     */
    public function testReversePairs()
    {
        $currencyPriceService = new CurrencyPriceService();
        $array = $currencyPriceService->reversePairs([]);
        $this->assertTrue(is_array($array));
    }

    /**
     * @group CurrencyPriceTest_getOutdatedPairs
     */
    public function testGetOutdatedPairs()
    {
        $currencyPriceService = new CurrencyPriceService();
        $array = $currencyPriceService->getOutdatedPairs([]);
        $this->assertTrue(is_array($array));
    }

    /**
     * @param  Blacklist $blacklist
     * @group CurrencyPriceTest_filterOutdatedPairs
     * @dataProvider filterPairsProvider
     */
    public function testFilterOutdatedPairs($blacklist)
    {
        /** @var MockObject|CurrencyPriceService $mockCurrencyPriceService */
        $mockCurrencyPriceService = $this
            ->getMockBuilder(CurrencyPriceService::class)
            ->setMethods([
                'reversePairsInExchanges',
                'getArrayDifference',
                'getReversePairsByExchanger',
                'getPairsBlacklist'
            ])
            ->getMock();

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('reversePairsInExchanges')
            ->willReturn([]);

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('getArrayDifference')
            ->willReturn([]);

        $mockCurrencyPriceService
            ->expects($this->once())
            ->method('getReversePairsByExchanger')
            ->willReturn([]);

        $mockCurrencyPriceService
            ->expects($this->any())
            ->method('getPairsBlacklist')
            ->willReturn(['dsdasas']);

        $result = $mockCurrencyPriceService->filterOutdatedPairs([], 'test_exchange', $blacklist);
        $this->assertTrue(is_array($result));
    }

    public function filterPairsProvider()
    {
        return [
            [
                'blacklist' => null
            ],
            [
                'blacklist' => new Blacklist(['pairs' => '{"Exchange":{"FROM":["TO"]}}'])
            ]
        ];
    }
}
